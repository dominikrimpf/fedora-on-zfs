# ZFS on Fedora

All commands are run as root unless otherwise stated.

## Notes


## Prepare
1. Boot live system
2. Download scripts:
   ```bash
   curl -o installzfs https://gitlab.com/domrim/fedora-on-zfs/-/raw/main/installzfs
   chmod +x installzfs
   curl -o zenter https://gitlab.com/domrim/fedora-on-zfs/-/raw/main/zenter
   chmod +x zenter
   curl -o zmount https://gitlab.com/domrim/fedora-on-zfs/-/raw/main/zmount
   chmod +x zmount
   ```
3. Source Information vars
   ```bash
   source /etc/os-release
   ```

## Setup from Live System
### Installation
1. Install zfs & needed packages into live image:
   ```bash
   ./installzfs
   ```

### Partitioning
1. set your Disk to environment variable
   ```bash
   export DISK=/dev/disk/by-id/YOUR_DISK_NAME
   ```
   with `ls -la /dev/disk/by-id` you can get a list of all disks on your system.

2. [OPTIONAL] Clear disk partition: `sgdisk --zap-all $DISK`
3. Create ESP Partition: `sgdisk -n0:1M:+1G -t0:EF00 $DISK`
4. [OPTIONAL] Create swap partition:
   ```bash
   export SWAPSIZE="8G" # Adjust size to your needs
   sgdisk -n0:0:+$SWAPSIZE -t0:8200 $DISK
   mkswap $DISK-part2
   swapon $DISK-part2
   ```
5. Create main partition:
   1. with swap
      ```bash
      sgdisk -n3:0:0 -t3:BF00 $DISK
      ```
   2. without swap
      ```bash
      sgdisk -n2:0:0 -t2:BF00 $DISK
      ```
6. Create pool:
   ```bash
   zpool create \
    -o ashift=12 \
    -O encryption=aes-256-gcm \
    -O keylocation=prompt -O keyformat=passphrase \
    -O acltype=posixacl -O canmount=off -O compression=lz4 \
    -O dnodesize=auto -O normalization=formD -O relatime=on \
    -O xattr=sa -O mountpoint=/ -R /mnt \
    rpool ${DISK}-part3 # replace "-part3" with "-part2" if you don't have configured a swap partition
   ```

### ZFS Setup
1. create main dataset
   ```bash
   zfs create -o canmount=off -o mountpoint=none rpool/ROOT
   ```
2. create root fs
   ```bash
   zfs create -o canmount=noauto -o mountpoint=/ rpool/ROOT/fedora
   zfs mount rpool/ROOT/fedora
   ```
3. create additional datasets
   ```bash
   zfs create rpool/home
   zfs create -o canmount=off rpool/var
   zfs create -o canmount=off rpool/var/lib
   zfs create rpool/var/log
   zfs create rpool/var/spool
   # Optional but recommended
   zfs create -o com.sun:auto-snapshot=false rpool/var/cache
   zfs create -o com.sun:auto-snapshot=false rpool/var/tmp
   # If you use docker
   zfs create -o com.sun:auto-snapshot=false rpool/var/lib/docker
   # If you use nfs (locking)
   zfs create -o com.sun:auto-snapshot=false rpool/var/lib/nfs
   ```

### Install Fedora
1. Install minimal base system
   ```bash
   dnf install -y --installroot=/mnt --releasever=$VERSION_ID @minimal-environment kernel-$(uname -r) kernel-modules-$(uname -r) kernel-modules-extra-$(uname -r) dosfstools
   ```
   We are installing the kernel from the live image here to avoid dkms conflicts when installing kernel / running `dracut` later.

2. Install zfs repositories
   ```bash
   dnf install -y --installroot=/mnt --releasever=$VERSION_ID http://download.zfsonlinux.org/fedora/zfs-release$(rpm -E %dist).noarch.rpm
   ```

3. Install zfs
   ```bash
   dnf install -y --installroot=/mnt --releasever=$VERSION_ID zfs zfs-dracut
   ```

### Configure System
1. Set hostname
   ```bash
   echo YOUR_HOSTNAME > /mnt/etc/hostname # replace YOUR_HOSTNAME
   ```

2. chroot into system
   ```bash
   ./zenter /mnt
   ```

3. remove Grub
   ```bash
   rpm --nodeps -ve $(rpm -qa | grep "^grub2-") os-prober
   echo 'exclude=grub2-*,os-prober' >> /etc/dnf/dnf.conf
   ```

4. setup systemd-boot
   ```bash
   rm -rvf /boot # Don't worry, we'll reinstall the kernel later
   mkdir boot # Create the boot folder
   mkdosfs -F 32 -s 1 -n EFI ${DISK}-part1 # You should not need to change this
   # If you want to use partition UUID's (more stable, but longer to type and slightly harder to debug)
   echo PARTUUID=$(blkid -s PARTUUID -o value ${DISK}-part1) \
      /boot vfat umask=0777,shortname=lower,context=system_u:object_r:boot_t:s0,nofail,x-systemd.device-timeout=1 0 1 >> /etc/fstab
   mount /boot
   mkdir -p /boot/$(</etc/machine-id)
   bootctl install # Install systemd-boot to ESP
   echo 'root=ZFS=rpool/ROOT/fedora rd.vconsole.keymap=de-latin1-nodeadkeys' > /etc/kernel/cmdline # replace keymap to your needs
   kernel-install add $(uname -r) /lib/modules/$(uname -r)/vmlinuz # Reinstall the kernel
   dracut --kver $(uname -r) --force --add-drivers "zfs" # Rebuild initramfs just in case
   ```

5. Set root password: `passwd`

6. check if `/etc/vconsole.conf` is configured correctly ([docs](https://www.freedesktop.org/software/systemd/man/vconsole.conf.html))

7. reboot into installed system :)

### First steps in new system
1. set keymap ([docs](https://docs.fedoraproject.org/en-US/fedora/rawhide/system-administrators-guide/basic-system-configuration/System_Locale_and_Keyboard_Configuration/))
   ```bash
   localectl status # get current statuisd
   localectl list-keymaps # get list of available keymaps
   localectl set-keymap MAP # replace MAP with your choice
   ```

2. set timezone ([docs](https://docs.fedoraproject.org/en-US/fedora/rawhide/system-administrators-guide/basic-system-configuration/Configuring_the_Date_and_Time/))
   ```bash
   timedatectl
   timedatectl list-timezones
   timedatectl set-timezone TIME_ZONE # replace TIME_ZONE with your choice
   timedatectl set-ntp yes # optional
   ```


### Random Notes
* selinux still makes problems (set in `/etc/selinux/config` `SELINUX` to `permissive` or `0`)
* Root password-set does not work initially, `restorecon` to the rescue. (check `/etc/*` with `matchpathcon`)
* After first boot an `fixfiles -F onboot` can solve some issues
* keyboard layout stuff is not tested



## Sources & Links
 * https://github.com/cheesycod/openzfs-docs-1/blob/master/docs/Getting%20Started/Fedora/Fedora%20Root%20On%20ZFS.rst
 * https://www.csparks.com/BootFedoraZFS/index.md
 * https://gist.github.com/p7cq/52f848b5c79ca9f16d814f96d601c745